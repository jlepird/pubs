function nsga2(fun,dimRange,lowerBounds,upperBounds,passArgs...)
#Minimizes all metrics!

	popsize = 100 #popsize, has to be even!
	gens = 50
	dimX = length(lowerBounds) #Dimension of domain

	#Generate Starting Population
	pop = rand(2*popsize,dimX)
	for i in 1:popsize
		pop[i,:] = pop[i,:] .* (upperBounds-lowerBounds) + lowerBounds
		pop[i + popsize,:] = pop[i + popsize,:] .* (upperBounds-lowerBounds) + lowerBounds
	end

	#Evaluate initial population's fitness
	fit = zeros(2*popsize,dimRange)
	for i = 1:popsize
		fit[i,:] = fun(pop[i,:],passArgs...)
	end

	const dimY = size(fit,2) #dimension of range

	for i = 1:gens
		if myid() == 1 #Don't print if we're parallizing
			println("Breeding and Testing Generation $i")
		end
		#Create the next generation
		couples = randperm(popsize)
		for j = 1:popsize
			#Stop only on every other iteration
			if j%2 == 1
				#New children
				p1 = pop[couples[j],:]
				p2 = pop[couples[j+1],:]
				pop[popsize + j,:] = sbx(p1,p2)'
				pop[popsize + j + 1 ,:] = sbx(p1,p2)'

				#Figure out children's fitness
				if sum(pop[popsize + j,:] .< lowerBounds) > 0 ||
				   sum(pop[popsize + j,:] .> upperBounds) > 0   #The kid's infeasible   
					fit[popsize + j,:] = Inf * ones(1,dimRange)
				else
					fit[popsize + j,:] = fun(pop[popsize+j,:],passArgs...)
				end
				if sum(pop[popsize + j + 1,:] .< lowerBounds) > 0 ||
				   sum(pop[popsize + j + 1,:] .> upperBounds) > 0   #The kid's infeasible
					fit[popsize + j + 1,:] = Inf * ones(1,dimRange)
				else
					fit[popsize + j + 1,:] = fun(pop[popsize + j + 1,:],passArgs...)
				end
			end
		end
		peckingorder = findPareto(fit)
		pop = pop[peckingorder,:]
		fit = fit[peckingorder,:]
	end
	return(pop,fit)
end

function sbx(p1,p2)
#Simulated binary crossover
	child = zeros(length(p1))
	for i = 1:length(p1)
	
		if rand() < 0.5
			foo = 1
		else
			foo = -1
		end
		
		child[i] = (p1[i]+p2[i])/2 + foo * findbeta() * abs(p1[i]-p2[i])/2
	end
	
	#Test if the kid is living near a nuclear power plant
	if rand() < 0.2 #yup
		child += rand(length(p1))*2.-1
	end
	
	return child
end

function findbeta()
	#Use inverse CDF method to find scaling factor beta
	u = rand()
	n = 3
	if u < 0.5
    	return (2*u)^(1/(n+1))
	else
    	return 2^(1/(-1-n)) * (1-u)^(1/(-1-n))
	end
end

function findPareto(fit)
	(popsize,dimY) = size(fit)
	popsize = div(popsize,2)

	#Find the domination fronts
	domCount = Inf * ones(2*popsize)#Each solutions domination front number
	domFoo = zeros(2*popsize)		#Num of unmatched solutions dominated member
	domSet = cell(2*popsize) 		#The set of solutions each solution dominates

	#Calculate initial dom numbers
	for i = 1:2*popsize
		domSet[i] = zeros(0)
		for j = 1:2*popsize
			if j != i
				if sum(fit[i,:] .< fit[j,:]) == dimY #Then i is better than j in all spects
					domFoo[j] += 1 #Add one to number of superior solns
					push!(domSet[i],j) #add j to i's domlist
				end
			end
		end
	end

	#Figure out which front each popmember is in
	assigned = 0
	front = 1
	while assigned < popsize
		frontDelta = zeros(2*popsize) #Change in each front's domFoo per iteration
		for i = 1:2*popsize
			if domFoo[i] == 0 #It's in the $front front
				domCount[i] = front #assign to front
				domFoo[i] = -1 #Don't look at again
				assigned += 1
				for j in domSet[i]
					frontDelta[j] += 1 #We'll decrease j's domFoo by 1 at the end of the loop
				end
			end
		end

		#Change domFoo, increase front
		domFoo = domFoo .- frontDelta
		front += 1
	end

	onEdge = find(domCount == front) #the pop members in the last front to be added
	crowdBox = zeros(length(onEdge),dimY)

	for j = 1:dimY
		dimFit = sortperm(fit[:,j])		
		for i = 1:length(onEdge)
			myRank = find(i==dimFit) #Where this population member is 
			if myRank == 1
				crowdBox[i,j] = abs(dimFit[1] - dimFit[2]) #Distance between 1,2
			elseif myRank == length(onEdge)
				crowdBox[i,j] = abs(dimFit[end] - dimFit[end-1])
			else
				crowdBox[i,j] = abs(dimFit[myRank] - dimFit[myRank-1]) + abs(dimFit[myRank] - dimFit[myRank + 1])
			end
		end
	end

	#For the last population front, add their spreadfactors to their domcount
	#This works because pop members not assigned a domfront have a domfront of Inf
	domCount[onEdge] += 1./prod(crowdBox,2)

	return sortperm(domCount)
end