type particle
	pos::Array{Float64,2}
	vel::Array{Float64,2}
	bestX::Array{Float64,2}
	bestY::Float64
end
type swarmBestType
	x::Array{Float64,2}
	y::Float64
end

function inertia(t)
	return 1/log(t+1)
end

# fun = cos
# lowerBound = ones(1,1) * pi/2
# upperBound = ones(1,1) * 3*pi/2
# passArgs = ()

function ps{T<:Real}(fun::Function,lowerBound::Array{T,2},upperBound::Array{T,2},passArgs...)
	const n = 100 #swarm size
	const tStop = 50 #number of time steps
	#Generate Starting swarm ullation
	const dX = length(lowerBound)
	const cCog = 1.47
	const cSoc = 1.47
	const control = 1
	const vMax = (upperBound-lowerBound)

	swarm = Array(particle,n)
	swarmBest = swarmBestType(ones(1,dX),Inf)

	#Generate intial swarm
	for i in 1:n
		swarm[i]=particle(
			rand(1,dX) .* (upperBound - lowerBound) .+ lowerBound, 
			(2*rand(1,dX) - 1) .* (upperBound-lowerBound),
			NaN * ones(1,dX), #Will quickly be overloaded, but will throw errors if something goes wrong later
			Inf)
		swarm[i].bestX = swarm[i].pos
		swarm[i].bestY = fun(swarm[i].pos,passArgs...)
		if swarm[i].bestY < swarmBest.y
			swarmBest.y = swarm[i].bestY
			swarmBest.x = swarm[i].bestX
		end
	end

	for t = 1:tStop
		for i in 1:n
			swarm[i].vel = control * (
				inertia(t) * swarm[i].vel +
				cCog .* rand(1,dX) .* (swarm[i].bestX .- swarm[i].pos) + 
				cSoc .* rand(1,dX) .* (swarmBest.x .- swarm[i].pos))
			swarm[i].vel = min(swarm[i].vel,vMax) #Cap velocities
			swarm[i].vel = max(swarm[i].vel,-vMax)
			swarm[i].pos += swarm[i].vel
			if sum(swarm[i].pos .< lowerBound) +
			   sum(swarm[i].pos .> upperBound) > 1
				val = Inf
			else
				val = fun(swarm[i].pos,passArgs...)
			end
			if val < swarmBest.y #Swarm Best 
				swarmBest.y = val
				swarmBest.x = swarm[i].pos
				swarm[i].bestX = swarm[i].pos
				swarm[i].bestY = val
			elseif val < swarm[i].bestY #Personal Best
				swarm[i].bestX = swarm[i].pos
				swarm[i].bestY = val
			end
		end
		println("Time $t Swarm Best: $(swarmBest.y)")
	end
	return (swarmBest.x,swarmBest.y)
end

ps{T<:Real}(fun::Function,lowerBound::Array{T,1},upperBound::Array{T,1},passArgs...) = ps(fun,lowerBound',upperBound',passArgs...)