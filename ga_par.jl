if nprocs() == 1
	addprocs(7)
end

require("par_wrapper.jl")

function ga_par{T<:Real}(fun::Function,lowerBound::Array{T,2},upperBound::Array{T,2},passArgs...)
	#Always minimizes!

	const n = 50 #people in population
	const gens = 50 #number of generations
	#Generate Starting Popullation
	pop = rand(2*n,length(lowerBound))
	for i in 1:n
		pop[i,:] = pop[i,:] .* (upperBound-lowerBound) + lowerBound
		pop[i + n,:] = pop[i + n,:] .* (upperBound-lowerBound) + lowerBound
	end

	evalCell = cell(n)

	#Evaluate initial population's fitness
	fit = zeros(2*n)
	for i = 1:n
		evalCell[i] = wrapperType(pop[i,:],fun,true,passArgs)
	end
	fit[1:n] = pmap(wrapMe,evalCell)
	for i = n+1:2n
		evalCell[i-n].x = pop[i,:]
	end
	fit[n+1:end] = pmap(wrapMe,evalCell)


	for i = 1:gens
		println("Generation $i Fitness: $(mean(fit[1:n]))")
		#Create the next generation
		couples = randperm(n)
		for j = 1:n
			#Stop only on every other iteration
			if j%2 == 1
				#New children
				p1 = pop[couples[j],:]
				p2 = pop[couples[j+1],:]
				pop[n + j,:] = sbx(p1,p2)'
				pop[n + j + 1 ,:] = sbx(p1,p2)'

				#Add to our eval group
				evalCell[j].x = pop[n+j,:]
				evalCell[j+1].x = pop[n+j+1,:]

				#Figure out children's fitness
				if sum(pop[n + j,:] .< lowerBound) > 0 ||
				   sum(pop[n + j,:] .> upperBound) > 0   #The kid's infeasible   
					evalCell[j].runMe = false
				else
					evalCell[j].runMe = true
				end
				if sum(pop[n + j + 1,:] .< lowerBound) > 0 ||
				   sum(pop[n + j + 1,:] .> upperBound) > 0   #The kid's infeasible
					evalCell[j+1].runMe = false
				else
					evalCell[j+1].runMe = true
				end
			end
		end
		
		#Distribute out the calculations!
		fit[n+1:end] = pmap(wrapMe,evalCell)

		#Sort the population. The bottom half gets overwritten in the next loop
		peckingorder = sortperm(fit)
		pop = pop[peckingorder,:]
		fit = fit[peckingorder]
		
	end
	return (pop[1,:],fit[1])
end

#Overload in case 1D bounds are added
ga_par{T<:Real}(fun::Function,lowerBound::Array{T,1},upperBound::Array{T,1},passArgs...) = ga_par(fun,lowerBound',upperBound',passArgs...)


function sbx(p1,p2)
#Simulated binary crossover
	child = zeros(length(p1))
	for i = 1:length(p1)
	
		if rand() < 0.5
			foo = 1
		else
			foo = -1
		end
		
		child[i] = (p1[i]+p2[i])/2 + foo * findbeta() * abs(p1[i]-p2[i])/2
	end
	
	#Test if the kid is living near a nuclear power plant
	if rand() < 0.2 #yup
		child += rand(length(p1))*2.-1
	end
	
	return child
end

function findbeta()
	#Use inverse CDF method to find scaling factor beta
	u = rand()
	n = 3
	if u < 0.5
    	return (2*u)^(1/(n+1))
	else
    	return 2^(1/(-1-n)) * (1-u)^(1/(-1-n))
	end
end